import org.postgresql.util.LruCache;

import java.sql.*;

public class Exercise8 {
    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Error loading driver: " + cnfe);
        }

        String host = "bronto.ewi.utwente.nl/";
        String dbName = "";
        String url = "jdbc:postgresql://" + host + dbName + "?currentSchema=movies";

        String username = "";
        String password = "";


        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();

            String query = "SELECT AuthorsOfMoviesWithActor(?)";

            connection.setAutoCommit(false);
            PreparedStatement st = connection.prepareStatement(query);
            st.setFetchSize(5);
            st.setString(1, "Bruce Willis");
            ResultSet resultSet = st.executeQuery();

            while(resultSet.next()) {
                System.out.println(resultSet.getString(1));
            }
            connection.close();
            resultSet.close();
        }
        catch(SQLException sqle) {
            System.err.println("Error connecting: " + sqle);
        }

    }
}

